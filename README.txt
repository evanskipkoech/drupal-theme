1. Create custom/SITE_NAME directory in DRUPAL_INSTALLATION/theme
2. Create info yml file SITE_NAME.info.yml
3. Create libraries file SITE_NAME.libraries.yml for libraries
4. To disable cache and enable twig debug copy example.settings.php from DRUPAL_INSTALLATION/sites to DRUPAL_INSTALLATION/sites/default
5. Rename example.settings.local.php to settings.local.php
6. Uncomment line 64
7. Uncomment block on line 712 in settings.php
8. Rebuild DRUPAL_INSTALLATION if you get an error
9. In DRUPAL_INSTALLATION/sites/default.services.yml create parameter to enable twig debug